
drop table if exists usuarios cascade;

CREATE TABLE usuarios (
    id serial primary key,
    username varchar(50),
    email varchar(128),
    full_name varchar(512),
    password varchar(256),
    enabled boolean
);

create unique index usuarios_login on usuarios(username);
