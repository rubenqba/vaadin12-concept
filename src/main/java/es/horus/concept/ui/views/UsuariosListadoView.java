package es.horus.concept.ui.views;

import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.router.Route;
import es.horus.concept.model.Usuario;
import es.horus.concept.service.UserService;
import es.horus.concept.ui.layout.AppLayout;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.util.StringUtils;
import org.vaadin.crudui.crud.CrudOperation;
import org.vaadin.crudui.crud.impl.GridCrud;

import java.util.Collection;

@Route(value = "usuarios", layout = AppLayout.class)
public class UsuariosListadoView extends ListadoView<Usuario> {

    private UserService userService;
    private PasswordEncoder encoder;

    public UsuariosListadoView(UserService userService, PasswordEncoder encoder) {
        this.userService = userService;
        this.encoder = encoder;
    }

    @Override
    protected GridCrud<Usuario> createGrid() {

        GridCrud<Usuario> grid = new GridCrud<>(Usuario.class, this);
        grid.getGrid().setColumns("id", "username", "email", "fullName");

        grid.getCrudFormFactory().setUseBeanValidation(true);
        grid.getCrudFormFactory().setVisibleProperties(CrudOperation.READ, "id", "username", "email", "fullName");
        grid.getCrudFormFactory().setVisibleProperties(CrudOperation.ADD, "username", "email", "fullName", "password");
        grid.getCrudFormFactory().setVisibleProperties(CrudOperation.UPDATE, "email", "fullName", "password");
        grid.getCrudFormFactory().setFieldType("password", PasswordField.class);

        return grid;
    }

    @Override
    public Collection<Usuario> findAll() {
        return userService.getUsuarios();
    }

    @Override
    public Usuario add(Usuario usuario) {
        usuario.setPassword(encoder.encode(usuario.getPassword()));
        userService.saveUsuario(usuario);
        return userService.getUsuario(usuario.getUsername()).orElse(null);
    }

    @Override
    public Usuario update(Usuario usuario) {
        if (StringUtils.hasText(usuario.getPassword())) {
            usuario.setPassword(encoder.encode(usuario.getPassword()));
        }
        userService.updateUsuario(usuario);
        return userService.getUsuario(usuario.getUsername()).orElse(null);
    }

    @Override
    public void delete(Usuario usuario) {
        userService.removeUsuario(usuario);
    }
}
