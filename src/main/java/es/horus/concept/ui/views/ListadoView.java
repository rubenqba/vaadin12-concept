package es.horus.concept.ui.views;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import lombok.Getter;
import org.vaadin.crudui.crud.CrudListener;
import org.vaadin.crudui.crud.impl.GridCrud;

public abstract class ListadoView<T> extends VerticalLayout implements CrudListener<T> {

    @Getter
    private GridCrud<T> grid;

    public ListadoView() {
        setSizeFull();
        setMargin(false);
        setPadding(false);
        addComponentAsFirst(defineGrid());
    }

    protected abstract GridCrud<T> createGrid();

    protected Component defineGrid() {
        grid = createGrid();
        return grid;
    }
}
