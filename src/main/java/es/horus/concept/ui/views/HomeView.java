package es.horus.concept.ui.views;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import es.horus.concept.ui.layout.AppLayout;

import java.time.Instant;
import java.util.Date;

@Route(value = "", layout = AppLayout.class)
public class HomeView extends VerticalLayout {

    public HomeView() {
        Button button = new Button(getTranslation("btn.clickme"),
                e -> Notification.show(getTranslation("btn.message", Date.from(Instant.now()))));
        add(button);
    }
}
