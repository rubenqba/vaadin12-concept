package es.horus.concept.ui.views;

import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.PWA;
import com.vaadin.flow.server.VaadinService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Locale;

@Route("login")
@PWA(name = "Project Base for Vaadin Flow with Spring", shortName = "Project Base")
@Slf4j
public class MainView extends VerticalLayout {

    private TextField usernameField;
    private PasswordField passwordField;
    private Checkbox remeberMeCheckbox;
    private Button loginButton;

    private Label logoutLabel;

    @Autowired
    private AuthenticationProvider authProvider;

    public MainView() {

        Locale l = VaadinService.getCurrentRequest().getLocale();
        LocaleContextHolder.setLocale(l);
        UI.getCurrent().setLocale(l);

        setAlignItems(Alignment.CENTER);

        logoutLabel = new Label();
        logoutLabel.getElement().getStyle().set("color", "red");
        logoutLabel.setVisible(false);
        add(logoutLabel);

        usernameField = new TextField();
        usernameField.setLabel(getTranslation("label.username"));

        passwordField = new PasswordField();
        passwordField.setLabel(getTranslation("label.password"));
        passwordField.addKeyPressListener(Key.ENTER, listener -> {
            log.debug("Enter pressed");
            loginAction();
        });

        remeberMeCheckbox = new Checkbox();
        remeberMeCheckbox.setLabel(getTranslation("label.rememberme"));

        loginButton = new Button(getTranslation("btn.login"));
        loginButton.addClickListener(e -> {
            log.debug("Login button pressed");
            loginAction();
        });
        add(usernameField, passwordField, remeberMeCheckbox, loginButton);
    }


    private void loginAction() {
        String username = usernameField.getValue();
        String password = passwordField.getValue();
        boolean rememberMe = remeberMeCheckbox.getValue();
        loginButton.setEnabled(false);
        if (login(username, password, rememberMe)) {
            loginButton.getUI().ifPresent(ui -> ui.navigate(HomeView.class));
        } else {
            logoutLabel.setVisible(true);
            loginButton.setEnabled(true);
        }
    }

    private boolean login(String username, String password, boolean rememberMe) {

        try {
            Authentication authenticate = authProvider.authenticate(new UsernamePasswordAuthenticationToken(username, password));
            if (authenticate.isAuthenticated()) {
                SecurityContext context = SecurityContextHolder.getContext();

                context.setAuthentication(authenticate);
                return true;
            }
        } catch (BadCredentialsException ex) {
            logoutLabel.setText(getTranslation("label.worng-password"));
            usernameField.focus();
            passwordField.setValue("");
        } catch (Exception ex) {
            Notification.show(getTranslation("text.try-again"), 3000, Notification.Position.MIDDLE);
            log.error("Unexpected error while logging in", ex);
        }
        return false;
    }

}
