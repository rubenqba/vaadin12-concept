package es.horus.concept.ui.layout;

import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.HasElement;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.RoutePrefix;
import com.vaadin.flow.router.RouterLayout;
import com.vaadin.flow.router.RouterLink;
import com.vaadin.flow.theme.Theme;
import com.vaadin.flow.theme.lumo.Lumo;
import es.horus.concept.config.SecurityUtils;
import es.horus.concept.ui.views.HomeView;
import es.horus.concept.ui.views.UsuariosListadoView;

import java.util.Objects;

import static com.vaadin.flow.component.orderedlayout.FlexComponent.Alignment.*;

@Theme(value = Lumo.class, variant = Lumo.LIGHT)
@HtmlImport("frontend://styles/shared-styles.html")
@RoutePrefix("sec")
public class AppLayout extends Composite<Div> implements RouterLayout {

    private Div header;
    private Div content;
    private Div footer;

    private HorizontalLayout menuBar;


    private VerticalLayout layout;

    public AppLayout() {
        buildLayout();
        getContent().setSizeFull();
        getContent().add(layout);
    }

    private void buildLayout() {

        VaadinIcon icon = VaadinIcon.VAADIN_H;
        Div userStatus = new Div(new Icon(icon));
        userStatus.setSizeFull();

        VerticalLayout userBar = new VerticalLayout(userStatus);
        userBar.setSizeFull();
        userBar.setAlignItems(END);

        menuBar = new HorizontalLayout(
                new RouterLink("Home", HomeView.class),
                new RouterLink("Usuarios", UsuariosListadoView.class)
        );
        menuBar.setAlignItems(BASELINE);
        menuBar.setWidth("100%");

        header = new Div(menuBar);
        header.setWidth("100%");

        footer = new Div(new Span("Horus derechos reservados - 2019"));
        content = new Div();
        content.setSizeFull();

        layout = new VerticalLayout(header, content, footer);
        layout.setSizeFull();
        layout.setAlignItems(CENTER);
    }

    @Override
    public void showRouterLayoutContent(HasElement hasElement) {
        System.out.println("showRouterLayoutContent - AppLayout");
        Objects.requireNonNull(hasElement);
        Objects.requireNonNull(hasElement.getElement());
        content.removeAll();
        content.getElement().appendChild(hasElement.getElement());
    }

    @Override
    protected void onAttach(AttachEvent attachEvent) {
        UI ui = getUI().get();

        if (SecurityUtils.isUserLoggedIn()) {

            Button button = new Button(getTranslation("btn.logout", SecurityUtils.getUsername()), event -> {
                // Redirect this page immediately
                ui.getPage().executeJavaScript(
                        "window.location.href='/logout'");
                // Close the session
                ui.getSession().close();
            });

            button.setIcon(new Icon(VaadinIcon.EXIT));
            button.getElement().getStyle().set("margin-left", "auto");
            menuBar.add(button);

            // Notice quickly if other UIs are closed
            ui.setPollInterval(3000);
        }
    }
}