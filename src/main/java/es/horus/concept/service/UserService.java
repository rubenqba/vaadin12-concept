package es.horus.concept.service;

import es.horus.concept.mapper.UsuarioMapper;
import es.horus.concept.model.Usuario;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class UserService {

    private UsuarioMapper usuarios;

    public UserService(UsuarioMapper usuarios) {
        this.usuarios = usuarios;
    }

    public Optional<Usuario> getUsuario(String username) {
        log.info("buscando usuario con nombre '{}'...", username);
        return Optional.ofNullable(usuarios.findByUsername(username));
    }

    public List<Usuario> getUsuarios() {
        log.info("recuperando todos los usuarios...");
        return usuarios.findAllUsers();
    }

    public void saveUsuario(Usuario usuario) {
        Optional<Usuario> found = getUsuario(usuario.getUsername());
        if (usuario.getId() == null && found.isPresent()) {
            usuario.setId(found.get().getId());
            updateUsuario(usuario);
        } else {
            usuarios.insertUser(usuario);
        }
    }

    public void updateUsuario(Usuario usuario) {
        if (usuario.getId() != null)
            usuarios.updateUser(usuario);
    }

    public void removeUsuario(Usuario usuario) {
        usuarios.deleteUser(usuario);
    }
}
