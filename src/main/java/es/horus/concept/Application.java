package es.horus.concept;

import es.horus.concept.model.Usuario;
import es.horus.concept.service.UserService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * The entry point of the Spring Boot application.
 */
@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public CommandLineRunner initData(UserService users, PasswordEncoder encoder) {
        return args -> {
            Usuario admin = new Usuario();
            admin.setUsername("admin");
            admin.setPassword(encoder.encode("admin"));
            admin.setFullName("Administrador del sistema");
            admin.setEmail("admin@horus.es");
            admin.setEnabled(true);
            users.saveUsuario(admin);

            Usuario user = new Usuario();
            admin.setUsername("user");
            admin.setPassword(encoder.encode("user"));
            admin.setFullName("Usuario del sistema");
            admin.setEmail("user@horus.es");
            admin.setEnabled(true);
            users.saveUsuario(admin);
        };
    }

}
