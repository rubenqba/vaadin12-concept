package es.horus.concept.mapper;

import es.horus.concept.model.Usuario;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface UsuarioMapper {

    @Select("SELECT * FROM usuarios WHERE username = #{username}")
    Usuario findByUsername(String username);

    @Select("select id, username, email, full_name, enabled from usuarios")
    List<Usuario> findAllUsers();

    @Insert("insert into usuarios (username, email, full_name, password, enabled) values(#{username}, #{email}, #{fullName}, #{password}, #{enabled})")
    void insertUser(Usuario usuario);

    @Update("update usuarios " +
            " set full_name = #{fullName}, " +
            "   email = #{email}, " +
            "   password = #{password} " +
            "   enabled = #{enabled} " +
            "where id = #{id}")
    void updateUser(Usuario usuario);

    @Delete(value = "delete from usuarios where id = #{id}")
    void deleteUser(Usuario usuario);
}
