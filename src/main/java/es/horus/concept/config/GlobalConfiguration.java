package es.horus.concept.config;

import es.horus.concept.mapper.UsuarioMapper;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GlobalConfiguration {

    private UsuarioMapper usuarios;

    public GlobalConfiguration(UsuarioMapper usuarios) {
        this.usuarios = usuarios;
    }

    @Bean
    CommandLineRunner printAdmin() {
        return (args) -> {
            System.out.println("buscando al administrador...");
            System.out.println(usuarios.findByUsername("admin"));
        };
    }

    @Bean
    CommandLineRunner listAll() {
        return (args) -> {
            System.out.println("recuperando a todos...");
            usuarios.findAllUsers().forEach(System.out::println);
        };
    }

}
