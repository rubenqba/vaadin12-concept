package es.horus.concept.config;

import es.horus.concept.service.UserService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class ConceptUserDetailService implements UserDetailsService {

    private UserService userService;

    public ConceptUserDetailService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public UserDetails loadUserByUsername(String usuario) throws UsernameNotFoundException {
        return userService.getUsuario(usuario).orElse(null);
    }
}
