# Creación del proyecto

La forma más sencilla de crear el proyecto desde cero es a partir de los inicializadores disponibles:

 - [Spring Initilizer](https://start.spring.io) 
 - [Vaadin Starter](https://vaadin.com/start)
 
En cualquiera de ellos se permite seleccionar el tipo de proyecto que se desea configurar y finalmente, descargar un esqueleto de proyecto funcional con las dependencias seleccionadas. En el caso del que brinda Spring se puede personalizar mucho más, ya que permite seleccionar a detalle los tipos de BD a utilizar, la tecnología con la que desea gestionar el acceso a datos y muchas otras alternativas de integración.

Este proyecto en particular se creó desde el Starter de Vaadin, pero en el camino me di cuenta que usando el de Spring se obtenía un mejor resultado ya que genera automaticamente configuraciones similares para Vaadin con el adicional de poder configurar toda la tecnología a usar en el backend desde el mismo Starter. En este proyecto, pues lo hice manualmente con las dependencias que ya sabía. 

# Configuración de dependencias

Las dependencias principales son gestionadas automáticamente por SpringBoot. 

 - Vaadin: La versión a utilizada es la `12.0.3` y se puede configurar en una properties dentro del `pom.xml`, automáticamente Spring configurará lo necesario por medio del starter correspondiente. 
   ```xml
    <dependency>
        <groupId>com.vaadin</groupId>
        <artifactId>vaadin-spring-boot-starter</artifactId>
    </dependency>
   ```
 - PostgreSQL: La versión de este driver es la `42.x` que ya cuenta con soporte para **PosgreSQL 10**
   ```xml
   <dependency>
       <groupId>org.postgresql</groupId>
       <artifactId>postgresql</artifactId>
       <scope>runtime</scope>
   </dependency>
   ```
 - MyBatis: Spring, por defecto, utiliza JPA para gestionar el acceso a los datos, pero incluyendo esta dependencia automáticamente se configura lo necesario para la conexión a la BD con soporte de configuración del framework. Por lo que cambiar entre JPA y MyBatis es tan simple como configurar la dependencia en el `pom.xml` y listo. Adicionalmente, hay un plugin de `Maven` que permite generar/actualizar la estructura y los datos de la BD cuando se usa MyBatis, pero no lo probé. 
   ```xml
   <dependency>
       <groupId>org.mybatis.spring.boot</groupId>
       <artifactId>mybatis-spring-boot-starter</artifactId>
       <version>1.3.2</version>
   </dependency>
    ```
 - Spring Security: gracias al soporte para autoconfiguración de SpringBoot, solo necesitamos incluir esta dependencia y ya tenemos soporte para aplicar un mínimo de seguridad tipo `Http BASIC` a nuestra aplicación y/o servicios Rest. En el caso particular de Vaadin hay que hacer algunas configuraciones adicionales para que pueda funcionar correctamente.
   ```xml
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-security</artifactId>
    </dependency>
   ``` 
   

Con esta configuración podemos ejecutar el esqueleto generado usando `mvn spring-boot:run` o ejecutando directamente la clase `Application` desde el IDE. Luego abrir [http://localhost:8080/]() en el navegador.

# Acceso a datos

Para el acceso a datos se configuran las propiedades del archivo `application.properties` o `application.yml` que se encuentran en el directorio `src/resources`. Spring soporta la configuración en cualquiera de los siguientes formatos: `XML`, `Property file`, `YAML`. Para este proyecto usé el formato `YAML` porque creo que es más intuitivo y menos verboso, además que es mas fácil reutilizar valores entre las propiedades. 

```yaml
spring:
  datasource:
    url: jdbc:postgresql://localhost/demo
    username: demo_user
    password: s3cr3t
    driver-class-name: org.postgresql.Driver
    initialization-mode: always
```

Con esto ya queda configurado automáticamente el acceso a datos. La base de datos la creé usando docker en mi propia computadora. Los datos de conexión que aparecen anteriormente son con los que me conecto y para crearla dejo en la raíz del proyecto el archivo [docker-compose.yml](docker-compose.yml) con las configuraciones necesarias. De igual forma pueden crear una BD donde deseen y cambiar la configuración según necesiten.  

Luego, se deben definir el modelo tal como se configuran en los demás proyectos que han desarrollado por lo que me voy a saltar esta configuración. En particular, no usé la configuración habitual en XML para ahorrar tiempo por la sencillez de las consultas, pero como comenté antes funciona de la misma forma que ya conocen.

Para poder usar el acceso a los datos se debe configurar una clase mapper por cada POJO que se tenga en la BD. MyBatis cuenta con una anotación `@Mapper` con la que podemos definir nuestros mappers y esta ya cuenta con soporte de autoconfiguración de Spring, por lo que podemos inyectar estos mappers en nuestros servicios u otros componentes de Spring de forma natural, por ejemplo:

```java
import es.horus.concept.model.Usuario;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface UsuarioMapper {

    @Select("SELECT * FROM usuarios WHERE username = #{username}")
    Usuario findByUsername(String username);

    @Select("select id, username, email, full_name, enabled from usuarios")
    List<Usuario> findAllUsers();

    @Insert("insert into usuarios (username, email, full_name, password, enabled) values(#{username}, #{email}, #{fullName}, #{password}, #{enabled})")
    void insertUser(Usuario usuario);

    @Update("update usuarios " +
            " set full_name = #{fullName}, " +
            "   email = #{email}, " +
            "   password = #{password} " +
            "   enabled = #{enabled} " +
            "where id = #{id}")
    void updateUser(Usuario usuario);

    @Delete(value = "delete from usuarios where id = #{id}")
    void deleteUser(Usuario usuario);
}
``` 

Se inyecta mediante el contexto de Spring de la siguiente forma: 

```java
import es.horus.concept.mapper.UsuarioMapper;
import es.horus.concept.model.Usuario;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class UserService {

    private UsuarioMapper usuarios;

    public UserService(UsuarioMapper usuarios) {
        this.usuarios = usuarios;
    }

    public Optional<Usuario> getUsuario(String username) {
        log.info("buscando usuario con nombre '{}'...", username);
        return Optional.ofNullable(usuarios.findByUsername(username));
    }

    public List<Usuario> getUsuarios() {
        log.info("recuperando todos los usuarios...");
        return usuarios.findAllUsers();
    }

    public void saveUsuario(Usuario usuario) {
        Optional<Usuario> found = getUsuario(usuario.getUsername());
        if (usuario.getId() == null && found.isPresent()) {
            usuario.setId(found.get().getId());
            updateUsuario(usuario);
        } else {
            usuarios.insertUser(usuario);
        }
    }

    public void updateUsuario(Usuario usuario) {
        if (usuario.getId() != null)
            usuarios.updateUser(usuario);
    }

    public void removeUsuario(Usuario usuario) {
        usuarios.deleteUser(usuario);
    }
}

```
   
# Interfaces de usuario

Vaadin 12 agrega un montón de nuevos componentes, un par de mejoras significativas en el rendimiento, una nueva opción de tema usando Material Design llamado [Vaadin Material](https://cdn.vaadin.com/vaadin-material-styles/1.2.0/demo/) y muchas otras pequeñas mejoras y correcciones de errores. Se dice que se hicieron algunas optimizaciones en Vaadin Flow, y ahora las aplicaciones desarrolladas con Vaadin 12 utilizan significativamente menos memoria por sesión. En lo personal a nivel de desarrollo si se ve que inicia más rápido, pero es difícil estar seguro porque no hay mucho que cargar tampoco. Un resumen de las mejoras puede encontrarse en este [video](https://www.youtube.com/watch?v=_d4I0g0yf_k). 

Las nuevas versiones de Vaadin permiten definir con anotaciones las rutas o endpoint donde se exponen las diferentes vistas. Todo esto se define con la anotación `@Route`. Además, algo que no conocía, ahora se permite aplicar un layout a las vistas de forma automática con estas anotaciones, lo que facilita muchísimo todo el tema del estilo genérico de las vistas. Por ejemplo, haciendo referencia a `Milenio`, con estos Layout se pueden implementar de una sola vez todas las opciones y detalles que aparecen en la parte superior de la aplicación y solo incluirla en un atributo de la anotación en todas las vistas que se desee. Eso facilita mucho la forma en que se hace en `Milenio` anidando una clase dentro de la otra para lograr el mismo efecto.  

En este caso yo hice uso de esto para probarlo, e incluí una barra de opciones en analogía a la que trae `Milenio` para cambiar entre opciones. Aquí muestro cómo hacerlo. 

Lo primero es definir el layout propiamente implementando la interfaz `RouteLayout`. Adicionalmente, se hereda en este caso del componente `Composite<Div>` ya que vamos a generar un componente compuesto a partir de un `div`. Dentro, podemos orquestar la interfaz que querramos. Esta, como mencioné antes, es una barra de navegación donde se encuentran un par de opciones: `Home` y `Usuarios`; además de un footer con los datos de copyright. 

```java
import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.HasElement;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.RoutePrefix;
import com.vaadin.flow.router.RouterLayout;
import com.vaadin.flow.router.RouterLink;
import com.vaadin.flow.theme.Theme;
import com.vaadin.flow.theme.lumo.Lumo;
import es.horus.concept.config.SecurityUtils;
import es.horus.concept.ui.views.HomeView;
import es.horus.concept.ui.views.UsuariosListadoView;

import java.util.Objects;

import static com.vaadin.flow.component.orderedlayout.FlexComponent.Alignment.*;

@Theme(value = Lumo.class, variant = Lumo.LIGHT)
@HtmlImport("frontend://styles/shared-styles.html")
@RoutePrefix("sec")
public class AppLayout extends Composite<Div> implements RouterLayout {

    private Div header;
    private Div content;
    private Div footer;

    private HorizontalLayout menuBar;


    private VerticalLayout layout;

    public AppLayout() {
        buildLayout();
        getContent().setSizeFull();
        getContent().add(layout);
    }

    private void buildLayout() {

        VaadinIcon icon = VaadinIcon.VAADIN_H;
        Div userStatus = new Div(new Icon(icon));
        userStatus.setSizeFull();

        VerticalLayout userBar = new VerticalLayout(userStatus);
        userBar.setSizeFull();
        userBar.setAlignItems(END);

        menuBar = new HorizontalLayout(
                new RouterLink("Home", HomeView.class),
                new RouterLink("Usuarios", UsuariosListadoView.class)
        );
        menuBar.setAlignItems(BASELINE);
        menuBar.setWidth("100%");

        header = new Div(menuBar);
        header.setWidth("100%");

        footer = new Div(new Span("Horus derechos reservados - 2019"));
        content = new Div();
        content.setSizeFull();

        layout = new VerticalLayout(header, content, footer);
        layout.setSizeFull();
        layout.setAlignItems(CENTER);
    }

    @Override
    public void showRouterLayoutContent(HasElement hasElement) {
        System.out.println("showRouterLayoutContent - AppLayout");
        Objects.requireNonNull(hasElement);
        Objects.requireNonNull(hasElement.getElement());
        content.removeAll();
        content.getElement().appendChild(hasElement.getElement());
    }

    @Override
    protected void onAttach(AttachEvent attachEvent) {
        UI ui = getUI().get();

        if (SecurityUtils.isUserLoggedIn()) {

            Button button = new Button(getTranslation("btn.logout", SecurityUtils.getUsername()), event -> {
                // Redirect this page immediately
                ui.getPage().executeJavaScript(
                        "window.location.href='/logout'");
                // Close the session
                ui.getSession().close();
            });

            button.setIcon(new Icon(VaadinIcon.EXIT));
            button.getElement().getStyle().set("margin-left", "auto");
            menuBar.add(button);

            // Notice quickly if other UIs are closed
            ui.setPollInterval(3000);
        }
    }
}
```

En este layout el contenido, se generará a partir de las vistas (`View`) que implementemos y que hagan uso de la clase anterior. Por ejemplo: 

```java
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import es.horus.concept.ui.layout.AppLayout;

import java.time.Instant;
import java.util.Date;

@Route(value = "", layout = AppLayout.class)
public class HomeView extends VerticalLayout {

    public HomeView() {
        Button button = new Button(getTranslation("btn.clickme"),
                e -> Notification.show(getTranslation("btn.message", Date.from(Instant.now()))));
        add(button);
    }
}
```

Una característica de los Layout es que nos permiten heredar también la sub-rutas que estas definen. En el caso del layout anterior se puede notar que mediante la anotación `@RoutePrefix("sec")` define que todas las vistas que la usen van a estar en la ruta `/sec`, por lo que la vista `HomeView` se encontrará en el path `/sec/home` relativo al `base_path` aplicación. 

## Listados y operaciones CRUD

Para el manejo de las operaciones CRUD en listados, opté por no inventar el hilo negro y utilicé un [add-on](https://vaadin.com/directory/component/crud-ui-add-on) del directorio de Vaadin que facilita la integración de estas funcionalidades comunes. El add-on tiene soporte para las versiones de Vaadin actuales (7,8, y 10+) y es totalmente configurable. 

![](https://static.vaadin.com/directory/user1373/screenshot/file2102953444183057054_1530141595507ScreenShot2018-06-28at2.16.26.png)

![](https://static.vaadin.com/directory/user1373/screenshot/file8009385866335923735_1530141629441ScreenShot2018-06-28at2.15.29.png)

![](https://static.vaadin.com/directory/user1373/screenshot/file8040804141294360474_1530141666295ScreenShot2018-06-28at2.17.02.png)


Para la implementación incluí una clase abstracta para poder manejar de forma común los diferentes tipos de objetos que se pudieran listar y que todos tuvieran el mismo diseño.

```java
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import lombok.Getter;
import org.vaadin.crudui.crud.CrudListener;
import org.vaadin.crudui.crud.impl.GridCrud;

public abstract class ListadoView<T> extends VerticalLayout implements CrudListener<T> {

    @Getter
    private GridCrud<T> grid;

    public ListadoView() {
        setSizeFull();
        setMargin(false);
        setPadding(false);
        addComponentAsFirst(defineGrid());
    }

    protected abstract GridCrud<T> createGrid();

    protected Component defineGrid() {
        grid = createGrid();
        return grid;
    }
}
```

Ejemplo de su empleo en la vista para listar los usuarios de la aplicación. Como puede verse es posible habilitar las opciones CRUD que se necesitan, así como los atributos del objeto que se desean mostrar/modificar en cada caso.

```java
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.spring.annotation.SpringComponent;
import es.horus.concept.model.Usuario;
import es.horus.concept.service.UserService;
import es.horus.concept.ui.layout.AppLayout;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.util.StringUtils;
import org.vaadin.crudui.crud.CrudOperation;
import org.vaadin.crudui.crud.impl.GridCrud;

import java.util.Collection;

@Route(value = "usuarios", layout = AppLayout.class)
public class UsuariosListadoView extends ListadoView<Usuario> {

    private UserService userService;
    private PasswordEncoder encoder;

    public UsuariosListadoView(UserService userService, PasswordEncoder encoder) {
        this.userService = userService;
        this.encoder = encoder;
    }

    @Override
    protected GridCrud<Usuario> createGrid() {

        GridCrud<Usuario> grid = new GridCrud<>(Usuario.class, this);
        grid.getGrid().setColumns("id", "username", "email", "fullName");

        grid.getCrudFormFactory().setUseBeanValidation(true);
        grid.getCrudFormFactory().setVisibleProperties(CrudOperation.READ, "id", "username", "email", "fullName");
        grid.getCrudFormFactory().setVisibleProperties(CrudOperation.ADD, "username", "email", "fullName", "password");
        grid.getCrudFormFactory().setVisibleProperties(CrudOperation.UPDATE, "email", "fullName", "password");
        grid.getCrudFormFactory().setFieldType("password", PasswordField.class);

        return grid;
    }

    @Override
    public Collection<Usuario> findAll() {
        return userService.getUsuarios();
    }

    @Override
    public Usuario add(Usuario usuario) {
        usuario.setPassword(encoder.encode(usuario.getPassword()));
        userService.saveUsuario(usuario);
        return userService.getUsuario(usuario.getUsername()).orElse(null);
    }

    @Override
    public Usuario update(Usuario usuario) {
        if (StringUtils.hasText(usuario.getPassword())) {
            usuario.setPassword(encoder.encode(usuario.getPassword()));
        }
        userService.updateUsuario(usuario);
        return userService.getUsuario(usuario.getUsername()).orElse(null);
    }

    @Override
    public void delete(Usuario usuario) {
        userService.removeUsuario(usuario);
    }
}
```

De igual forma, para poder enrutar las funcionalidades relacionadas con la recuperación de los datos a mostrar el add-on brinda una interfaz donde se configuran los listener para cada operación CRUD. En este caso las implementé en la misma clase de la vista pero pueden perfectamente ser atendidas por cualquier componente o servicio de Spring o custom. 

# Seguridad

Spring Security viene con una cantidad increíble de opciones y, por defecto, con incluirlo en el classpath del proyecto incluye un mínimo de seguridad configurando un formulario de login en HTML básico y la configuracion mediante el esquema BASIC. 

Para poder funcionar con Vaadin se deben configurar algunos detalles en los que Spring Boot nos ayuda mucho. La documentación de esto está en la clase [SecurityConfiguration](src/main/java/es/horus/concept/config/SecurityConfiguration.java) 

La vista de configuración se define en la clase [MainView](src/main/java/es/horus/concept/ui/views/MainView.java) y contiene solo un formulario simple de login y las instrucciones necesarias para cargar los datos de la sesión de usuario en el contexto de seguridad de Spring. De ahí en adelante el framework se encarga de todo.  